document.addEventListener('DOMContentLoaded', function() {
    var contactButton = document.getElementById('contact_form');
    var overlay = document.getElementById('overlay');
    var closeButton = document.getElementById('close');
    contactButton.addEventListener('click', function(e) {
        e.preventDefault();
        overlay.classList.add('overlay-show')
    })

    closeButton.addEventListener('click', function() {
        overlay.classList.remove('overlay-show');
    })


    var switcherMenu = document.getElementById('switch_mobile_menu');
    var sidebarMobile = document.getElementById('sidebarMobile');
    var closeMenu = document.getElementById('close_menu');

    switcherMenu.addEventListener('click', function() {
        sidebarMobile.classList.toggle('active');
    })
    closeMenu.addEventListener('click', function() {
        switcherMenu.click();
    })

    /**************** */

    var ths = document.querySelectorAll('th[class^="column"]');
    var tds = document.querySelectorAll('td[class^="column"');
    var title_heads = [];
    ths.forEach(function(th) {
        title_heads.push(th.innerHTML);
    })

    var columnNumber = 0;
    tds.forEach(function(td) {
        td.setAttribute('data-label', title_heads[columnNumber]);
        columnNumber++;
        if (columnNumber === 5) columnNumber = 1;
    }) 
})